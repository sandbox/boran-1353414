
Given a list of nodes, create a block with a menu of node titles on the right, and a space on the left to load the relevant node via ajax. Looks like an interactive slideshow a bit.

This was used for a specific site and may serve an example for others.

Settings: add variable to settings.php and tweak the css !


Sean Boran (boran), http://drupal.org/user/147185

